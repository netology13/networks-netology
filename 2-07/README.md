# Домашнее задание к занятию 2.7 Настройка коммутации и маршрутизации.

### Лабораторная работа "Настройка коммутации и маршрутизации"

Соберите в среде Packet Tracer приведенную на схеме топологию и выполните задания.

![image](https://user-images.githubusercontent.com/71018632/146266900-f142fb82-dd1b-4248-a966-f84154c94254.png)
---------------------------------------------------------------------------------------------------------------

### Задание 1.

Создать VLANы пользователей 100, 200, 300 на коммутаторах. Создать соответствующие VLAN интерфейсы. Назначить IP адреса на АРМ пользователей. Проверить связанность между пользователями внутри VLAN, между разными VLAN и доступность шлюзов в каждом VLAN.

*Результат представить в виде вывода команд `show vlan brief` и `show interface trunk` с коммутаторов SW_1, SW_2, SW_3.*

---

### Ответ:

Построенная в соотвествии с заданием топология представлена ниже:

![topology](.README_images/topology.png)

Show vlan brief **SW_1**:

```commandline
SW_1#sh vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
100  USER_100                         active    Fa0/1
200  USER_200                         active    Fa0/2
300  USER_300                         active    Fa0/3
```

Show interface trunk **SW_1**:

```commandline
SW_1#sh int trunk
Port        Mode         Encapsulation  Status        Native vlan
Gig0/1      on           802.1q         trunking      1

Port        Vlans allowed on trunk
Gig0/1      100,200,300

Port        Vlans allowed and active in management domain
Gig0/1      100,200,300

Port        Vlans in spanning tree forwarding state and not pruned
Gig0/1      100,200,300
```

Show vlan brief **SW_2**:

```commandline
SW_2#sh vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
100  USER_100                         active    Fa0/1
200  USER_200                         active    Fa0/2
300  USER_300                         active    Fa0/3
```

Show interface trunk **SW_2**:

```commandline
SW_2#sh int trunk
Port        Mode         Encapsulation  Status        Native vlan
Gig0/1      on           802.1q         trunking      1

Port        Vlans allowed on trunk
Gig0/1      100,200,300

Port        Vlans allowed and active in management domain
Gig0/1      100,200,300

Port        Vlans in spanning tree forwarding state and not pruned
Gig0/1      100,200,300
```

Show vlan brief **SW_3**:

```commandline
SW_3>sh vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
100  USER_100                         active  
200  USER_200                         active  
222  ROUTER                           active    Gig1/1/1
300  USER_300                         active  
```

Show interface trunk **SW_3**:

```commandline
SW_3#sh int trunk
Port        Mode         Encapsulation  Status        Native vlan
Gig1/0/1    on           802.1q         trunking      1
Gig1/0/2    on           802.1q         trunking      1

Port        Vlans allowed on trunk
Gig1/0/1    100,200,300
Gig1/0/2    100,200,300

Port        Vlans allowed and active in management domain
Gig1/0/1    100,200,300
Gig1/0/2    100,200,300

Port        Vlans in spanning tree forwarding state and not pruned
Gig1/0/1    100,200,300
Gig1/0/2    100,200,300
```

### Задание 2.

Настроить связанность между SW_3 и R_1. Настроить связанность между SRV_100 и R_1.

*Результат представить в виде вывода команды с R_1 `show ip route` и с SW_3 `show vlan brief` и `show ip route`.*

---

### Ответ:

Show ip route **R_1**:

```commandline
Router#show ip route

Gateway of last resort is not set

     10.0.0.0/8 is variably subnetted, 3 subnets, 2 masks
S       10.10.0.0/24 [1/0] via 192.168.0.2
C       10.100.0.0/24 is directly connected, FastEthernet0/0
L       10.100.0.1/32 is directly connected, FastEthernet0/0
     192.168.0.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.0.0/30 is directly connected, GigabitEthernet0/1/0
L       192.168.0.1/32 is directly connected, GigabitEthernet0/1/0
```

Show vlan brief **SW_3**:

```commandline
SW_3#show vlan brief

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gig1/0/3, Gig1/0/4, Gig1/0/5, Gig1/0/6
                                                Gig1/0/7, Gig1/0/8, Gig1/0/9, Gig1/0/10
                                                Gig1/0/11, Gig1/0/12, Gig1/0/13, Gig1/0/14
                                                Gig1/0/15, Gig1/0/16, Gig1/0/17, Gig1/0/18
                                                Gig1/0/19, Gig1/0/20, Gig1/0/21, Gig1/0/22
                                                Gig1/0/23, Gig1/0/24, Gig1/1/2, Gig1/1/3
                                                Gig1/1/4
100  USER_100                         active    
200  USER_200                         active    
222  ROUTER                           active    Gig1/1/1
300  USER_300                         active    
1002 fddi-default                     active    
1003 token-ring-default               active    
1004 fddinet-default                  active    
1005 trnet-default                    active    
```

Show ip route **SW_3**:

```commandline
SW_3#sh ip ro

Gateway of last resort is not set

     10.0.0.0/26 is subnetted, 3 subnets
C       10.10.0.0 is directly connected, Vlan100
C       10.10.0.64 is directly connected, Vlan200
C       10.10.0.128 is directly connected, Vlan300
     192.168.0.0/30 is subnetted, 1 subnets
C       192.168.0.0 is directly connected, Vlan222
```

### Задание 3.

Настроить связанность на Site_3 аналогично Site_1 и проверить связанность между серверами SRV_1 и 2, а так же доступность R_2 с серверов.

*Результат представить в виде вывода команд *`show ip route` с R_2 и `show vlan brief` с SW_5.*

---

### Ответ:

Show ip route **R_2**:

```commandline
Router#sh ip ro

Gateway of last resort is 192.168.0.5 to network 0.0.0.0

     10.0.0.0/24 is subnetted, 1 subnets
S       10.11.0.0/24 [1/0] via 192.168.0.10
     192.168.0.0/24 is variably subnetted, 4 subnets, 2 masks
C       192.168.0.4/30 is directly connected, GigabitEthernet0/1/0
L       192.168.0.6/32 is directly connected, GigabitEthernet0/1/0
C       192.168.0.8/30 is directly connected, GigabitEthernet0/3/0
L       192.168.0.9/32 is directly connected, GigabitEthernet0/3/0
S*   0.0.0.0/0 [1/0] via 192.168.0.5
```

Show vlan brief **SW_5**:

```commandline
SW_5(config-if)#do sh vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gig1/0/3, Gig1/0/4, Gig1/0/5, Gig1/0/6
                                                Gig1/0/7, Gig1/0/8, Gig1/0/9, Gig1/0/10
                                                Gig1/0/11, Gig1/0/12, Gig1/0/13, Gig1/0/14
                                                Gig1/0/15, Gig1/0/16, Gig1/0/17, Gig1/0/18
                                                Gig1/0/19, Gig1/0/20, Gig1/0/21, Gig1/0/22
                                                Gig1/0/23, Gig1/0/24, Gig1/1/2, Gig1/1/3
                                                Gig1/1/4
222  ROUTER                           active    Gig1/1/1
661  SRV_1                            active    Gig1/0/1
662  SRV_2                            active    Gig1/0/2
1002 fddi-default                     active    
1003 token-ring-default               active    
1004 fddinet-default                  active    
1005 trnet-default                    active  
```

## Дополнительное задание (со звездочкой*)

Эти задания дополнительные (не обязательные к выполнению) и никак не повлияют на получение вами зачета по этому домашнему заданию. Вы можете их выполнить, если хотите глубже и/или шире разобраться в материале.

### Задание 4*.

С помощью статических маршрутов на R_1 и R_2 обеспечьте связанность между всеми сегментами на всех Site.

*Результат представить в виде вывода команд `show ip route` с R_1 и R_2*

---

### Ответ:
Show ip route **R_1**:

```commandline
Router#sh ip ro

Gateway of last resort is 192.168.0.6 to network 0.0.0.0

     10.0.0.0/8 is variably subnetted, 3 subnets, 2 masks
S       10.10.0.0/24 [1/0] via 192.168.0.2
C       10.100.0.0/24 is directly connected, FastEthernet0/0
L       10.100.0.1/32 is directly connected, FastEthernet0/0
     192.168.0.0/24 is variably subnetted, 4 subnets, 2 masks
C       192.168.0.0/30 is directly connected, GigabitEthernet0/1/0
L       192.168.0.1/32 is directly connected, GigabitEthernet0/1/0
C       192.168.0.4/30 is directly connected, GigabitEthernet0/2/0
L       192.168.0.5/32 is directly connected, GigabitEthernet0/2/0
S*   0.0.0.0/0 [1/0] via 192.168.0.6
```

Show ip route **R_2**:

```commandline
Router#sh ip ro

Gateway of last resort is 192.168.0.5 to network 0.0.0.0

     10.0.0.0/24 is subnetted, 1 subnets
S       10.11.0.0/24 [1/0] via 192.168.0.10
     192.168.0.0/24 is variably subnetted, 4 subnets, 2 masks
C       192.168.0.4/30 is directly connected, GigabitEthernet0/1/0
L       192.168.0.6/32 is directly connected, GigabitEthernet0/1/0
C       192.168.0.8/30 is directly connected, GigabitEthernet0/3/0
L       192.168.0.9/32 is directly connected, GigabitEthernet0/3/0
S*   0.0.0.0/0 [1/0] via 192.168.0.5
```