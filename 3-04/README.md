# Домашнее задание к занятию "Протоколы FHRP"

### Кейс

У вашей компании есть центральный офис и филиал. В центральном офисе коммутатор и два маршрутизатора. В филиале один коммутатор и один маршрутизатор.
До филиала построено два канала L3. Один канал основной и второй канал - резервный.
Резервный канал - дорогой, оплата по количеству трафика, пользоваться им необходимо по возможности реже.
При этом необходимо автоматическое переключение в случае отказа основного канала и возврат в исходное состояние при восстановлении связи.
Сеть офиса - 192.168.0.0/24
Сеть филиала - 192.168.1.0/24
Основной канал - 10.0.1.0/30
Резервный канал - 10.0.2.0/30

### Задание 1

Уточнение - оборудование __Juniper__

1. Выбрать протокол из семейтсва FHRP и обосновать свой выбор.
2. Нарисовать схему сети.
3. Настроить маршрутизаторы центрального офиса в соответствии с выбранным протоколом.

*Ответы на вопрос 1 привести в свободной форме, в текстовом виде.
На вопрос 2 - приложить файл в формате .png или .jpg.
На вопрос 3 - привести настройки интерфейсов в текстовом виде*

---

### Ответ:

1. С учётом того что используется оборудование Juniper используем протокол VRRP.

2. ![scheme1](.README_images/scheme1.png)

3. 

```commandline
R1# set interfaces ge 0/0/1 unit o family inet address 192.168.0.4/24 vrrp-group 192 192.168.0.254
R1# set interfaces ge 0/0/1 unit o family inet address 192.168.0.4/24 vrrp-group 192 priority 110
R1# set interfaces ge 0/0/1 unit o family inet address 192.168.0.4/24 vrrp-group 192 track interface ge-0/0/0 priority-cost 20
R1# set interfaces ge 0/0/1 unit o family inet address 192.168.0.4/24 vrrp-group 192 accept-data
R1# set interfaces ge 0/0/1 unit o family inet address 192.168.0.4/24 vrrp-group 192 preempt
R1# set interfaces ge 0/0/0 and o family inet address 10.0.1.1/30
R1# set routing-options static route 192.168.1.0/24 next-hop 10.0.1.2
```

```commandline
R2# set interfaces ge 0/0/1 unit o family inet address 192.168.0.5/24 vrrp-group 192 192.168.0.254 set interfaces ge 0/0/1 unit o family inet address 192.168.0.5/24 vrrp-group 192 accept-data
R2# set interfaces ge 0/0/0 unit o family inet address 10.0.2.1/30
R2# set routing-options static route 192.168.1.0/24 next-hop 10.0.2.2
```

```commandline
R3# set interfaces ge 0/0/0 unit o family inet address 10.0.1.2/30 set interfaces ge 0/0/1 unit o family inet address 10.0.2.2/30
R3# set interfaces ge 0/0/2 unit o family inet address 192.168.1.1/24
R3# set routing-options static route 192.168.0.0/24 next-hop 10.0.1.1 set routing-options static route 0.0.0.0/0 next-hop 10.0.2.1
```

### Задание 2. Лабораторная работа "Выбор и настройка протокола из семейства FHRP"

Компания из кейса, но все оборудование __Cisco__.

1. Выбрать протокол из семейства FHRP и обосновать свой выбор.
2. Построить топологию в Сisco Packet Tracer.
3. Настроить оборудование центрального офиса и филиала.
4. Проверить работу резервирования связи с филиалом. Для этого отключить основной канал, проверить командами ping и tracert доступность ПК в филиале и путь до него.

*На вопрос 1 - ответ в свободной форме, в текстовом виде.
На вопросы 2 и 3 - приложить .pkt файл.
На вопрос 4 - скриншоты выполнения команд ping и tracert.*

---

### Ответ:

1. С учётом того что используется оборудование Cisco используем проприетарный протокол вендора - HSRP.

2. ![PKT-файл](task2.pkt)

3. ![ping_1](.README_images/ping_1.png)

### Задание 3

Компания та же, но в центральном офисе добавили еще один маршрутизатор. Оборудование __Cisco__.
Адресация для каналов Интернет:

1. 10.1.0.0/30
2. 10.2.0.0/30
3. 10.3.0.0/30

Для имитации сети Интернет можно добавить еще один маршрутизатор, к которому подключить все три канала интернет.
На этом же маршрутизаторе создать Loopback интерфейс с адресом 10.10.10.10/32 и использовать этот адрес для проверки доступности сети Интернет.

Необходимо обеспечить компании доступ в Интернет. Непрерывно и с балансировкой по трем каналам.

1. Выбрать протокол из семейства FHRP и обосновать свой выбор.
2. Нарисовать схему сети.
3. Настроить маршрутизаторы центрального офиса в соответствии с выбранным протоколом.

*Ответы на вопрос 1 привести в свободной форме, в текстовом виде.
На вопрос 2 - приложить файл в формате .png или .jpg.
На вопрос 3 - привести настройки интерфейсов в текстовом виде*

### Ответ:

1. Ввиду увеличившегося количества роутеров используем протокол GLBP.

2. ![scheme2](.README_images/scheme2.png)

3. Настройка GLBP:

```commandline
R1 (config)# interface gigabitEthernet 0/0
R1 (config-if)# ip address 192.168.0.4 255.255.255.0
R1 (config-if)# glbp 192 ip 192.168.0.254
R1 (config-if)# glbp 192 weighting 103
R1 (config-if)# glbp 192 authentication text SecKey1
```

```commandline
R2 (config)# interface gigabitEthernet 0/0
R2 (config-if)# ip address 192.168.0.5 255.255.255.0
R2 (config-if)# glbp 192 ip 192.168.0.254
R2 (config-if)# glbp 192 priority 200
R2 (config-if)# glbp 192 load-balancing weighted
R2 (config-if)# glbp 192 preempt 
R2 (config-if)# glbp 192 weighting track 1 decrement 10
R2 (config-if)# glbp 192 weighting 105 lower 95 upper 105
R2 (config-if)# glbp 192 authentication text SecKey1
```

```commandline
R3 (config)# interface gigabitEthernet 0/0
R3 (config-if)# ip address 192.168.0.6 255.255.255.0
R3 (config-if)# glbp 192 ip 192.168.0.254
R3 (config-if)# glbp 192 weighting 90
R3 (config-if)# glbp 192 priority 90
R3 (config-if)# glbp 192 authentication text SecKey1
```

```commandline
R4 (config)# interface loopback 0
R4 (config-if)# ip address 10.10.10.10 255.255.255.255
```
