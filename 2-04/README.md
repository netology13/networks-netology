# Домашнее задание к занятию "2.4	Маршрутизация. Шлюз по умолчанию.  Выбор лучшего маршрута."

### Задание 1. Лабораторная работа "Выбор наилучшего маршрута на базе статической маршрутизации"

Соберите тестовую сеть, настройте статическую маршрутизацию для тестовой сети согласно топологии.

- Проверьте работоспособность сети.
- Измените настройки административной дистанции так, чтобы пакеты маршрутизировались через путь R0 - R3 - R2.

Сделайте вывод, как административная дистанция влияет на маршрут.

<img width="737" alt="Снимок экрана 2021-11-23 в 17 45 04" src="https://user-images.githubusercontent.com/73060384/143046501-32c03615-6ea6-4bd8-a925-5ad11234b65a.png">

*Приведите ответ в свободной форме и пришлите вывод show ip route на всех трех маршрутизаторах.*

---

### Ответ:

**R0 show ip route:**

```commandline
Gateway of last resort is 192.168.0.2 to network 0.0.0.0

     192.168.0.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.0.0/24 is directly connected, Serial0/1/0
L       192.168.0.1/32 is directly connected, Serial0/1/0
     192.168.1.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.1.0/24 is directly connected, Serial0/1/1
L       192.168.1.1/32 is directly connected, Serial0/1/1
     192.168.10.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.10.0/24 is directly connected, GigabitEthernet0/0/0
L       192.168.10.2/32 is directly connected, GigabitEthernet0/0/0
S    192.168.20.0/24 [1/0] via 192.168.0.2
S*   0.0.0.0/0 [1/0] via 192.168.0.2 
```

**R2 show ip route:**

```commandline
Gateway of last resort is not set

     192.168.1.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.1.0/24 is directly connected, Serial0/1/0
L       192.168.1.2/32 is directly connected, Serial0/1/0
     192.168.2.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.2.0/24 is directly connected, Serial0/1/1
L       192.168.2.2/32 is directly connected, Serial0/1/1
S    192.168.10.0/24 [1/0] via 192.168.2.1
     192.168.20.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.20.0/24 is directly connected, GigabitEthernet0/0/0
L       192.168.20.2/32 is directly connected, GigabitEthernet0/0/0
```

**R3 show ip route:**

```commandline
Gateway of last resort is not set

     192.168.0.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.0.0/24 is directly connected, Serial0/1/0
L       192.168.0.2/32 is directly connected, Serial0/1/0
     192.168.2.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.2.0/24 is directly connected, Serial0/1/1
L       192.168.2.1/32 is directly connected, Serial0/1/1
S    192.168.10.0/24 [1/0] via 192.168.0.1
S    192.168.20.0/24 [1/0] via 192.168.2.2
```

![route](.README_images/route.gif)

### Задание 2. Лабораторная работа "Выбор наилучшего маршрута на базе динамической маршрутизации"

Соберите тестовую сеть, настройте протокол маршрутизации rip для тестовой сети согласно топологии.

- Проверьте работоспособность сети.
- Подключите R6 и R7 маршрутизаторы в разрыв линии R2-R4 маршрутизаторов. Вместо соединения R2-R4 должно быть R2-R6-R7-R4.
- Выполните их настройку.
- Протестируйте полученную сеть.

Как подключение маршрутизаторов повлияло на метрики выбора маршрута и почему, какие выводы вы можете сделать до и после подключения.

<img width="468" alt="image" src="https://user-images.githubusercontent.com/73060384/142831858-71671547-a415-4d74-bb09-469de2367f4a.png">

*Приведите ответ в свободной форме и пришлите pkt файл.*

---

### Ответ:

![pkt-файл](5.pkt)

**Метрика по маршруту от R2 до 192.168.20.1 в первой конфигруации R2-R4(64)-R5(1)-PC1(1) = 66**

![config1](.README_images/config1.png)

```commandline
R2#sh ip ro 192.168.20.1
Routing entry for 192.168.20.0/24
Known via "ospf 11", distance 110, metric 66, type intra area
  Last update from 192.168.1.2 on Serial0/1/0, 00:01:15 ago
  Routing Descriptor Blocks:
  * 192.168.1.2, from 192.168.20.2, 00:01:15 ago, via Serial0/1/0
      Route metric is 66, traffic share count is 1
```

**Метрика по маршруту от R2 до PC1 во второй конфигруации R2-R3(64)-R4(64)-R5(1)-PC1(1) = 130**

![config2](.README_images/config2.png)

```commandline
R2#sh ip ro 192.168.20.1
Routing entry for 192.168.20.0/24
Known via "ospf 11", distance 110, metric 130, type intra area
  Last update from 192.168.6.1 on Serial0/1/1, 00:10:57 ago
  Routing Descriptor Blocks:
  * 192.168.6.1, from 192.168.20.2, 00:10:57 ago, via Serial0/1/1
      Route metric is 130, traffic share count is 1
```

Вывод: стоимость кратчайшего маршрута от R2 до PC1 увеличилась т.к. появилось дополнительное Serial подключение
стоимостью 100000000/1544000 = 64.76 = 64
