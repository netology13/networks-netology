# Домашнее задание к занятию 2.6 Путь пакета в коммутируемой и маршрутизируемой среде. ARP, DHCP, DNS.

### Задание 1.

На основном интерфейсе вашего ПК (или виртуальной машины) посмотрите дамп трафика через tshark или Wireshark.

*Приведите скриншоты, где показаны основные моменты работы протокола ARP и DHCP.*

---

### Ответ:

Работа протокола ARP:

![ARP_request](.README_images/ARP_request.png)

DHCP Discover:

![DHCP_discover](.README_images/DHCP_discover.png)

DHCP Offer:

![DHCP_offer](.README_images/DHCP_offer.png)

DHCP request:

![DHCP_request](.README_images/DHCP_request.png)

DHCP ACK:

![DHCP_ACK](.README_images/DHCP_ACK.png)

### Задание 2.

Как настраивается для сетевого пакета размер MTU в сетях, использующих только протокол IPv6?

*Приведите ответ в свободной форме.*

### Ответ:

В IPv6 минимальный размер MTU составляет 1280 байт. Следовательно, пакеты IPv6, размер которых меньше этого ограничения, не разбиваются на фрагменты. Для передачи пакетов IPv6 по линии связи с размером MTU меньше 1280 байт эти пакеты должны разбиваться и собираться на уровне канала связи.

---

### Задание 3. Лабораторная работа "Построение сети и разбор передаваемых в ней пакетов"

1. В Cisco Packet Tracer соберите сеть состоящую из двух маршрутизаторов (R1 и R2), за каждым из которых есть коммутатор (Switch1 и Switch2), а за коммутатором по два компьютера (Comp1, Comp2 и Comp3, Comp4). Все устройства этой сети должны быть доступны между собой.

Сетевые настройки можно использовать следующие:

- R1 10.1.1.1/27
- R2 10.1.2.1/27

*Приведите скриншоты таблицы коммутации и таблицы маршрутизации устройств R1, R2, Switch1, Switch2.
Пришлите pkt файл.*

2. Как будут выглядеть заголовки пакета на каждом из узловых точек сети из первой части лабораторного задания при обмене данными Comp1 с Comp4?

*Приведите ответ в свободной форме.*

---

### Ответ:

Построенная сеть:

![network](.README_images/network.png)

Маршрутизация на R1:

```commandline
R1#sh ip ro
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

     192.168.1.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.1.0/24 is directly connected, GigabitEthernet0/0/0
L       192.168.1.1/32 is directly connected, GigabitEthernet0/0/0
     192.168.2.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.2.0/24 is directly connected, GigabitEthernet0/0/1
L       192.168.2.1/32 is directly connected, GigabitEthernet0/0/1
S    192.168.3.0/24 [1/0] via 192.168.2.2
```

Маршрутизация на R2:

```commandline
Router#sh ip ro
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

S    192.168.1.0/24 [1/0] via 192.168.2.1
     192.168.2.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.2.0/24 is directly connected, GigabitEthernet0/0/1
L       192.168.2.2/32 is directly connected, GigabitEthernet0/0/1
     192.168.3.0/24 is variably subnetted, 2 subnets, 2 masks
C       192.168.3.0/24 is directly connected, GigabitEthernet0/0/0
L       192.168.3.1/32 is directly connected, GigabitEthernet0/0/0
```

Таблица коммутации Switch1:

```commandline
Switch1#show mac-address-table 
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----

   1    0002.17aa.3001    DYNAMIC     Fa0/3
   1    000a.f327.3d9b    DYNAMIC     Fa0/2
   1    0050.0f87.a2ea    DYNAMIC     Fa0/1
```

Таблица коммутации Switch2:

```commandline
Switch2#show mac-address-table 
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----

   1    0001.421c.4e31    DYNAMIC     Fa0/1
   1    0001.4358.a138    DYNAMIC     Fa0/2
   1    0060.3ea3.b201    DYNAMIC     Fa0/3
```

Содержимое пакета на каждой точке сети:

![comp1_packet](.README_images/comp1_packet.png)

![switch1_packet](.README_images/switch1_packet.png)

![r1_packet](.README_images/r1_packet.png)

![r2_packet](.README_images/r2_packet.png)

![comp4_packet](.README_images/comp4_packet.png)

[pkt-файл](7.pkt)

### Задание 4*.

К лабораторной работе из задания №3 добавьте сервер DNS за любым из маршрутизаторов.

- Имя: Netology.ru
- Тип: A Запись

*Приведите скриншоты, где с помощью утилиты nslookup можно получить IP адрес DNS-сервера.*

![nslookup](.README_images/nslookup.png)
